package com.silverline.Cafs.BaseTests;

import java.util.concurrent.TimeUnit;
import java.io.File;

import com.silverline.Cafs.Methods.MainMethods;
import com.silverline.Cafs.Pages.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTests {

    public static WebDriver driver;
    public static ClientDetails cd;
    public static BuildHousehold bh;
    public static Opportunity opp;
    public static MainMethods mm;
    public static Total t;

    //@BeforeClass
    /*
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "D:\\2018_!!!\\comsilverlineCafs\\chromedriver.exe");
        driver = new ChromeDriver();
        cd = new ClientDetails(driver);
        bh = new BuildHousehold(driver);
        opp = new Opportunity(driver);
        mm = new MainMethods(driver);
        t = new Total(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        driver.get("https://test.salesforce.com/?un=silverline.cacu@silverlinecrm.com.cafs.test&pw=Silverline18");
        mm.WaitForHomePage();
    }
    */
    @BeforeClass
    public static void setup() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platform", "WIN10");
        caps.setCapability("version", "67");
        caps.setCapability("browserName", "chrome");
        //caps.setCapability("name", "My First Test");
        //System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
        //File file = new File("chromedriver");
        cd = new ClientDetails(driver);
        bh = new BuildHousehold(driver);
        opp = new Opportunity(driver);
        mm = new MainMethods(driver);
        t = new Total(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        driver.get("https://test.salesforce.com/?un=silverline.cacu@silverlinecrm.com.cafs.test&pw=Silverline18");
        mm.WaitForHomePage();
    }

    @AfterClass
    public static void end() {
        driver.quit();
    }
}
