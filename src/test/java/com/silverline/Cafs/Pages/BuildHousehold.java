package com.silverline.Cafs.Pages;

import com.silverline.Cafs.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuildHousehold extends BaseTests{

    public WebDriver driver;

    public BuildHousehold(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public static String ErrorMessageForRelatedLegalEntity = "Required fields are missing: [Related Legal Entity]";

    //---FIELDS
    @FindBy(xpath ="//input[@data-interactive-lib-uid='17']")
    public WebElement RelatedLegalEntityField;

    @FindBy(xpath ="//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']")
    public WebElement RelatedLegalEntityFirstValue;

    @FindBy(xpath ="//select[@data-interactive-lib-uid='18']")
    public WebElement RelationshipPicklist;

    @FindBy(xpath ="//label[@for ='input-22']/span[@class='slds-checkbox_faux']")
    public WebElement IsImmediateFamilyCheckbox;

    @FindBy(xpath ="//label[@for ='input-23']/span[@class='slds-checkbox_faux']")
    public WebElement IsDependentCheckbox;

    @FindBy(xpath ="//label[@for ='input-24']/span[@class='slds-checkbox_faux']")
    public WebElement IsBeneficiaryCheckbox;

    //---Add New Account
    @FindBy(xpath =".slds-listbox__option-text_entity")
    public WebElement AddNewAccountButton;

    @FindBy(xpath ="//div[@id='modal-content-id-1']/form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[2]/lightning-input//input[@type='text']")
    public WebElement AddNewAccountFirstName;

    @FindBy(xpath ="//div[@id='modal-content-id-1']/form[@id='form-wrapper']/div[@class='slds-grid slds-wrap']/div[3]/lightning-input//input[@type='text']")
    public WebElement AddNewAccountLastName;

    @FindBy(css ="[id=\"modal-content-id-1\"] .slds-m-vertical--medium")
    public WebElement AddNewAccountSubmitButton;

    //---BUTTONS
    @FindBy(css =".cSL_cmp_AccordionElement:nth-child(2) .SL_cmp_AccordionElementBody .cSL_cmp_DynamicForm .slds-m-vertical--medium")
    public WebElement SubmitButtonOnBuildHouseholdPage;

    @FindBy(xpath ="//div[@id='brandBand_1']/div/div[1]//div[@class='cSL_cmp_AccordionForm']/div/div[2]/div[@class='SL_cmp_AccordionElementFooter']/button[1]")
    public WebElement PreviousButtonOnBuildHouseholdPage;

    @FindBy(css =".cSL_cmp_AccordionElement:nth-child(2) .SL_cmp_AccordionElementFooter .SL_cmp_AccordionElementButton-next")
    public WebElement NextButtonOnBuildHouseholdPage;

    //---METHODS
    public void GoToBuildHouseholdPage(){
        cd.GoToClientDetailsPage();
        mm.FillField(cd.LastNameField, "TestName"+Math.random());
        mm.NavigateToNextPage(cd.SubmitButtonOnClientDetailsPage, cd.SolutationDataPageTwo);
        mm.NavigateToNextPage(cd.NextButtonOnClientDetailsPageTwo, bh.RelatedLegalEntityField);
    }

    public void FillFieldsOnBuildHouseholdPage() throws InterruptedException {
        mm.SelectFromSearchField(bh.RelatedLegalEntityField, bh.RelatedLegalEntityFirstValue, cd.FirstName+" "+cd.LastName+cd.LN2);
        mm.SelectFromPicklist(bh.RelationshipPicklist, 2);
        mm.ClickOn(bh.IsImmediateFamilyCheckbox);
        mm.ClickOn(bh.IsDependentCheckbox);
        mm.ClickOn(bh.IsBeneficiaryCheckbox);
    }
}
